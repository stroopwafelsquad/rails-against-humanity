# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Add all cards to the Card database
require 'net/http'
require 'json'

begin
  # Created this myself using http://www.crhallberg.com/cah/json
  data = JSON.parse(Net::HTTP.get(URI("http://pastebin.com/raw/86gDNnLq")))

  prepare_pack = -> (str) { str.gsub(/(\&\w+\;)*\.*(\[.+\]\s)*/, '') }
  expand_underscore = -> (str) { str.gsub(/_/, '______') }

  data.keys[2..-2].each do |set|
    set = data[set]
    ["black", "white"].each do |color|
      set[color].each do |i|
        card = data["#{color}Cards"][i]
        Card.create(pack: prepare_pack.call(set["name"]),
                    color: color,
                    text: expand_underscore.call(card["text"] || card),
                    pick: card["pick"])
      end
    end
  end
rescue SocketError
  puts "Can't seed cards due to (most likely) no internet connection."
end

["Test Game 1", "Test Game 2", "Test Game 3"].each do |game|
  Game.create(name: game, password: 'password')
end
