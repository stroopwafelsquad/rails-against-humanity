class AddStoreToGame < ActiveRecord::Migration
  def change
    add_column :games, :settings, :string, :limit => 4096
  end
end
