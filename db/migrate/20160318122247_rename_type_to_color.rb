class RenameTypeToColor < ActiveRecord::Migration
  def change
    rename_column :cards, :type, :color
  end
end
