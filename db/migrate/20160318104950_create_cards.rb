class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :pack
      t.string :type
      t.string :text
      t.integer :pick

      t.timestamps null: false
    end

    add_index :cards, :pack
    add_index :cards, :type
  end
end
