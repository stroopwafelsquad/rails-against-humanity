class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name
      t.string :password_digest
      t.boolean :password_protected

      t.timestamps null: false
    end
  end
end
