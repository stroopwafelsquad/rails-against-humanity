class RemovePasswordProtectedCol < ActiveRecord::Migration
  def change
    remove_column :games, :password_protected
  end
end
