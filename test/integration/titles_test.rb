require 'test_helper'

class TitlesTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  test "correct titles on all pages" do
    get '/'
    assert_select 'title', full_title
    get '/instructions'
    assert_select 'title', full_title('Instructions')
    get '/about'
    assert_select 'title', full_title('About')
  end

  test "sets the 'active' class to correct li element" do
    get '/'
    assert_select '.active', {count: 1, text: "Play"}
    get '/instructions'
    assert_select '.active', {count: 1, text: "Instructions"}
    get '/about'
    assert_select '.active', {count: 1, text: "About"}
  end
end
