require 'test_helper'

class PackViewerControllerTest < ActionController::TestCase
  def setup
    @available_packs = Card.available_packs
  end

  test "should show all packs on overview page" do
    get :overview
    assert_select ".pack a", @available_packs.length
  end

  test "pack pages should have correct amount of cards" do
    @available_packs.each do |pack|
      get :show_pack, {name: pack}
      [:black, :white].each do |color|
        assert_select ".#{color}-card", Card.get_by_pack(pack)[color].length
      end
    end
  end

  test "accessing a pack that doesn't exist should give error" do
    assert_raises(ActionController::RoutingError) { get :show_pack, {name: "non-existent"} }
  end
end
