require 'test_helper'

class GameControllerTest < ActionController::TestCase
  def setup
    @host = SecureRandom.base64
    @test_game = Game.create(name: "game used in testing #1", password: 'password')
    @test_game.players = []
    @test_game.save
  end

  test "should create new game with correct parameters and redirect to new game" do
    assert_difference 'Game.count' do
      post :create, {name: "game used in testing", password: 'password'}
    end
    assert_redirected_to controller: "game", action: "game", name: Game.last.name
  end

  test "should be able to leave existing game" do
    @test_game.create(@host, nil)
    @test_game.save!
    assert_difference 'Game.count', -1 do
      delete :leave, {game: {name: @test_game.name}, id: @host}
    end
    assert_redirected_to root_url
  end

  test "shouldn't be able to leave game you're not in" do
    id = SecureRandom.base64
    @test_game.players << SecureRandom.base64 # not same as above
    @test_game.save!
    assert_no_difference 'Game.count' do
      delete :leave, {game: {name: @test_game.name}, id: id}
      @test_game.reload
    end
    assert_redirected_to root_url
    assert_equal @test_game.players.length, 1
  end

  test "should be able to join game with correct pass" do
    @test_game.last_kicked = nil
    @test_game.save
    post :game, {name: @test_game.name, password: "password"}
    assert_redirected_to action: "game", name: "game used in testing #1"
  end

  test "shouldn't be able to join game with wrong pass" do
    post :game, {name: @test_game.name, password: "wrong password"}
    assert_redirected_to root_url
  end

  test "should be able to kick player as host" do
    other_player = SecureRandom.base64
    @test_game.create(@host, nil)
    session[:identity] = @host # for testing

    assert_difference '@test_game.players.length', 1 do
      @test_game.update_players(other_player)
    end

    assert_difference '@test_game.players.length', -1 do
      delete :kick, {name: @test_game.name, player: other_player}
      @test_game.reload
    end

    assert_redirected_to action: "game", name: @test_game.name
  end

  test "should be able to play card" do
    other_player = SecureRandom.base64
    @test_game.create(@host, nil)

    @test_game.update_players(other_player)

    not_czar_sample = not_czar(@test_game).sample

    session[:identity] = not_czar_sample

    assert_difference '@test_game.round.values.flatten.length' do
      post :play_card, {name: @test_game.name, player: not_czar_sample, card: @test_game.decks[not_czar_sample].sample}
      @test_game.reload
    end
  end

  test "czar should be able to select card" do
    other_player = SecureRandom.base64
    @test_game.create(@host, nil)
    session[:identity] = @host # czar

    @test_game.update_players(other_player)

    assert @test_game.card_czar == @host, "first player to join should be czar"

    not_czar_sample = not_czar(@test_game).sample

    @test_game.get_black_card.pick.times do
      @test_game.play_card(not_czar_sample, @test_game.decks[not_czar_sample].sample)
    end

    assert_difference '@test_game.scores[not_czar_sample]' do
      post :select_card, {cards: @test_game.round[not_czar_sample].to_json, from: not_czar_sample, name: @test_game.name}
      @test_game.reload
    end
  end
end
