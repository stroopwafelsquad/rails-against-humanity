ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def not_czar(game)
    game.players.reject { |p| p == game.card_czar }
  end
end

module FixtureHelpers
  def random_packs
    @random_packs ||= Array.new(5).map do; Faker::Hacker.adjective; end
    @random_packs
  end
end

# use the helper methods from FixtureHelpers in fixtures
ActiveRecord::FixtureSet.context_class.send :include, FixtureHelpers
