require 'test_helper'

class CardTest < ActiveSupport::TestCase
  def setup
    @available_packs = Card.available_packs
  end

  test "should get all available packs" do
    assert_not_nil @available_packs
  end

  test "no packs should be empty" do
    @available_packs.each do |pack|
      assert_not_nil Card.get_by_pack pack
    end
  end
end
