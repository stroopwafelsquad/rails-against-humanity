require 'test_helper'

class GameTest < ActiveSupport::TestCase
  def setup
    @test_game = Game.new
    @host = SecureRandom.base64
  end

  def create_game(packs=nil)
    @test_game.name = Faker::Name.name
    @test_game.save
    @test_game.create(@host, packs)
  end

  test "name should be required" do
    @test_game.password = 'password'
    assert_not @test_game.valid?
  end

  test "should be able to auth with correct pass" do
    @test_game.name = "game used in test #1"
    @test_game.password = 'password'
    assert @test_game.valid?
    assert_not @test_game.authenticate("wrong password")
    assert @test_game.authenticate("password")
  end

  test "name should not be empty" do
    @test_game.name = ""
    assert_not @test_game.valid?
  end

  test "name should not be > 50 chars" do
    @test_game.name = "k" * 51
    assert_not @test_game.valid?
  end

  test "assigns packs properly when no req. packs are passed in" do
    create_game

    assert @test_game.packs.size > 0
  end

  test "assigns decks properly" do
    create_game

    assert @test_game.decks[@host].length == Game::DECK_SIZE
  end

  test "assigns new black card when doesn't exist" do
    create_game

    assert_not @test_game.black_card.nil?
  end

  test "should be ready for new round when cards have been played" do
    create_game

    id2 = SecureRandom.base64

    @test_game.update_players(id2)

    # pretend the user played all the required cards
    @test_game.get_black_card.pick.times do
      [@host, id2].each { |id| @test_game.round[id] << "test card" }
    end

    @test_game.save!

    assert @test_game.ready_for_next_round?
  end

  test "should assign new czar and host if player leaves/is kicked" do
    create_game

    id2 = SecureRandom.base64
    id3 = SecureRandom.base64
    id4 = SecureRandom.base64

    [id2, id3, id4].each { |id| @test_game.update_players id }

    assert @test_game.host == @host, "first player should be host"
    assert @test_game.card_czar == @test_game.players[0], "czar should also be first player to join"

    @test_game.delete_player(@host)

    assert @test_game.host == @test_game.players[0], "host is the second player to join"
    assert @test_game.players.include?(@test_game.card_czar), "card czar should be a random player"
  end

  test "should be able to play a card" do
    create_game

    id2 = SecureRandom.base64
    id3 = SecureRandom.base64
    id4 = SecureRandom.base64

    [id2, id3, id4].each { |id| @test_game.update_players id }

    assert @test_game.round.values.flatten.length == 0, "shouldn't be any values in the round"

    @test_game.play_card(@host, @test_game.decks[@host].sample)

    assert_not @test_game.round[@host].empty?, "host should have played a card"
  end

  test "czar should be able to select a card and point should be awarded" do
    create_game

    id2 = SecureRandom.base64

    @test_game.update_players id2

    not_czar_sample = not_czar(@test_game).sample

    assert_difference '@test_game.waiting_for_players.length', -1 do
      @test_game.get_black_card.pick.times do
        @test_game.play_card(not_czar_sample, @test_game.decks[not_czar_sample].sample)
      end
    end

    assert_difference '@test_game.scores[not_czar_sample]' do
      @test_game.select_card(@test_game.round[not_czar_sample], not_czar_sample)
    end
  end

  test "if new czar is assigned and that person has played cards, he should get the cards back" do
    create_game
    id2 = SecureRandom.base64
    id3 = SecureRandom.base64
    @test_game.update_players id2
    @test_game.update_players id3

    not_czar(@test_game).each {|p| @test_game.play_card(p, @test_game.decks[p].sample)} until @test_game.waiting_for_players.length == 1

    @test_game.delete_player(@test_game.card_czar)

    assert @test_game.round[@test_game.card_czar] == []
    assert @test_game.decks[@test_game.card_czar].size == Game::DECK_SIZE
  end

  test "should be able to determine card packs properly when passed in req. packs" do
    packs_to_req = Card.available_packs.map { |p| {p => "1"} }.reduce(&:merge)

    # create some packs that don't exist
    10.times do
      packs_to_req[Faker::Lorem.word] = rand(2)
    end

    create_game(packs_to_req)

    assert_equal Card.available_packs.length, @test_game.packs.length
    assert_equal Card.available_packs, @test_game.packs
  end

  test "should default to random available pack when invalid packs are passed in" do
    packs_to_req = Array.new(10).map { |p| {Faker::Lorem.word => "1"} }.reduce(&:merge)

    create_game(packs_to_req)

    assert @test_game.packs.length > 0
    assert Card.available_packs.include? @test_game.packs.first # only one
  end

  test "should add another pack if not enough blacks or whites" do
    packs_to_req = {"blacks_only" => "1"}

    create_game(packs_to_req)

    assert Card.get_by_pack(@test_game.packs)[:white].length > 0
    assert Card.get_by_pack(@test_game.packs)[:black].length > 0
  end
end
