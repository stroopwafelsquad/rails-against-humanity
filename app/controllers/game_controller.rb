class GameController < ApplicationController
  before_action :gen_identity
  before_action :get_current_game, except: [:create]
  before_action :authenticate, only: [:game]
  before_action :redir_if_kicked, except: [:create, :set_nick]

  def game
    if @game.last_kicked == session[:identity] && !@game.players.include?(session[:identity])
      @game.last_kicked = nil
      @game.save
      flash[:alert] = 'You were kicked from the game.'
      return redirect_to root_url
    end
    @game.create(session[:identity], nil) if @game.host.nil?
    @game.update_players(session[:identity])
    @game.save
    return redirect_to controller: 'game', action: 'game', name: @game.name if request.request_method == 'POST'
  end

  def create
    begin
      new_game = Game.new(params.permit(:name, :password))
      new_game.create(session[:identity], params[:pack])
      flash[:notice] = 'Game created.'
      session[:password_digest] = new_game.password_digest
      redirect_to controller: 'game', action: 'game', name: new_game.name
    rescue
      flash[:alert] = 'Game creation failed.'
      redirect_to root_url
    end
  end

  def leave
    begin
      raise if !@current_game.delete_player(params[:id])
      flash[:notice] = "Left game \"#{@current_game.name}\"."
    rescue
      flash[:alert] = 'Could not leave game.'
    end

    redirect_to root_url
  end

  def kick
    if @current_game.host == session[:identity]
      begin
        @current_game.delete_player(params[:player])
        @current_game.last_kicked = params[:player]
        @current_game.save!
        flash[:notice] = "Kicked player #{params[:player]}"
      rescue
        flash[:alert] = "Player doesn't exist"
      end
    else
      flash[:alert] = "You're not authorized to perform that action."
    end

    respond_to do |format|
      format.html { redirect_to action: 'game', name: @current_game.name }
      format.js
    end
  end

  def select_card
    if @current_game.card_czar == session[:identity] && @current_game.waiting_for_players.length == 1
      begin
        @current_game.select_card(JSON.parse(params[:cards]), params[:from])
      rescue
        flash[:alert] = "Could not award point for \"#{params[:cards]}\""
      end
    else
      flash[:alert] = 'Could not select card'
    end

    respond_to do |format|
      format.html { redirect_to action: 'game', name: params[:name] }
      format.js
    end
  end

  def play_card
    @game = @current_game
    if session[:identity] != params[:player]
      flash[:alert] = 'That is not your card. You broke something.'
    elsif session[:identity] == @current_game.card_czar
      flash[:alert] = "You're the card czar."
    elsif !@current_game.waiting_for_players.include? params[:player]
      flash[:alert] = 'You have played all cards required this round'
    else
      if !@current_game.play_card(params[:player], params[:card])
        flash[:alert] = 'Error while playing card.'
      end
    end

    respond_to do |format|
      format.html { redirect_to action: 'game', name: params[:name] }
      format.js
    end
  end

  def get_players
    @game = @current_game

    respond_to do |format|
      format.html { render plain: @game.players }
      format.js
    end
  end

  def get_cards
    @game = @current_game
    respond_to do |format|
      format.js
    end
  end

  def set_nick
    # remove special chars and limit length
    req_nick = params[:nick].gsub(/[^\w\s]/, '')[0...50] rescue ''

    if req_nick.empty?
      flash[:alert] = 'Requested nick can not be empty'
      return respond_to { |format| format.js }
    end

    if @current_game.players.include? req_nick
      flash[:alert] = 'That nickname is already in use'
    else
      @current_game.migrate_nick(session[:identity], req_nick)
      session[:identity] = req_nick
    end

    return respond_to { |format| format.js }
  end

  private

  def redir_if_kicked
    if @current_game.last_kicked == session[:identity] && !@current_game.players.include?(session[:identity])
      @current_game.last_kicked = nil
      @current_game.save
      flash[:alert] = 'You were kicked from the game.'
      return render js: "window.location = '#{root_url}'"
    end
  end

  def get_current_game
    req_game = params[:name] || params[:game][:name]
    @current_game ||= Game.where('name=?', req_game).first rescue nil
  end

  def gen_identity
    session[:identity] ||= Faker::Name.name
  end

  def authenticate
    authenticate_game = Game.where('name=?', params[:name]).first

    begin
      @game = authenticate_game.authenticate(params[:password])
      if !@game
        @game = authenticate_game if authenticate_game.password_digest == session[:password_digest]
      end
      raise if !@game
      session[:password_digest] = @game.password_digest
    rescue
      flash[:alert] = 'Wrong password'
      return redirect_to root_url
    end
  end
end
