class PackViewerController < ApplicationController
  def overview
    @title = "Packs"
    @packs = Card.available_packs
  end

  def show_pack
    @title = params[:name]
    @pack_name = params[:name]
    @pack = Card.get_by_pack @pack_name
    not_found("Card pack doesn't exist") if @pack[:white].size + @pack[:black].size == 0
  end
end
