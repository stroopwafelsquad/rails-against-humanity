class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def not_found(msg="Not Found")
    raise ActionController::RoutingError.new(msg)
  end
end
