class StaticPagesController < ApplicationController
  def index
    @games = Game.order(:updated_at).reverse_order
    @packs = Card.available_packs
  end

  def instructions
    @title = 'Instructions'
  end

  def about
    @title = "About"
  end

  # ajax
  def get_games
    @games = Game.order(:updated_at).reverse_order
    respond_to do |format|
      format.js
    end
  end
end
