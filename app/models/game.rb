class Game < ActiveRecord::Base
  include BCrypt

  validates :name, {presence: true, uniqueness: true, length: (1..50)}

  # this is a horrible implementation, and should definitely never have seen the light
  # please look away before your eyes burn away, and you slowly transform into a puddle of sorrow and depression
  store :settings, accessors: [:black_card, :players, :host,
                               :card_czar, :packs, :decks,
                               :round, :scores, :last_kicked,
                               :last_winner, :last_nick_change], coder: JSON

  DECK_SIZE = 8

  #Tyler the
  def create(creator, requested_packs)
    # throw error if the game isn't valid according to model
    raise "Game is not valid" if !self.valid?
    self.host = self.card_czar = creator
    self.players = [creator]
    self.last_winner = []
    self.decks = self.round = self.scores = {}
    self.last_nick_change = ["", ""]
    self.last_kicked = nil
    self.save!

    determine_packs(requested_packs)

    update_players(creator)
  end

  # add all vars to new user
  def update_players(id=nil)
    # only add if it's not there already
    self.players << id if !self.players.include?(id) if id

    self.players.each do |p|
      self.decks[p] ||= []
      self.round[p] ||= []
      self.scores[p] ||= 0
    end

    self.save

    # don't distribute if no player is specified
    distribute_cards if (self.decks[id].empty? rescue false)

    if ready_for_next_round?
      new_round
    end
  end

  def delete_player(id)
    self.players.delete(id)
    self.decks.delete(id)
    self.round.delete(id)
    self.scores.delete(id)

    if self.card_czar == id and self.players.length > 1
      self.card_czar = self.players.sample
      new_round
    end

    self.host = self.players.first if self.host == id

    self.save!

    self.delete if self.players.length == 0

    return self.players.include?(id) ? false : true
  end

  def new_round
    self.black_card = nil
    self.save
    get_black_card
    self.round.keys.each { |k| self.round[k] = [] }
    distribute_cards
    self.card_czar = self.players[self.players.index(self.card_czar).next] || self.players[0]
    self.save
  end

  def play_card(player, card)
    # if user doesn't exist
    return nil if !self.players.include? player
    # if user doesn't have card
    return nil if !self.decks[player].include? card

    self.round[player] << card
    # only remove first entry, in case of multiple of same card
    self.decks[player].delete_at(self.decks[player].index(card))

    self.save

    return true
  end

  def select_card(cards, from_player)
    cards.each { |c| raise if !self.round[from_player].include? c }
    self.scores[from_player] += 1
    self.last_winner = cards
    self.save
    new_round
  end

  # has everyone done what they should do?
  def ready_for_next_round?
    # waiting_for_players returns a list of names
    waiting_for_players.size == 0
  end

  # deal a new black card
  def get_black_card
    if self.black_card.nil? or ready_for_next_round?
      self.black_card = Card.get_by_pack(self.packs)[:black].sample.id
    end

    self.save!

    return Card.where("id=?", self.black_card).first
  end

  def determine_packs(requested_packs)
    available_packs = Card.available_packs

    self.packs = requested_packs.to_a.reject do |x|
      x.last == "0" or !available_packs.include? x.first
    end.map { |x| x[0] } # only return the name, not the checked (1 or 0)

    if self.packs.nil? or self.packs.empty?
      self.packs = [Card.available_packs.sample]
    end

    cards_to_be = -> {Card.get_by_pack(self.packs)}

    self.packs << Card.available_packs.sample until cards_to_be.call[:white].length != 0 and cards_to_be.call[:black].length != 0

    self.save!
  end

  def distribute_cards
    self.players.each do |player|
      loop do
        # if the player somehow has more than the requested amount of cards
        if self.decks[player].length > DECK_SIZE
          self.decks[player].pop until self.decks[player] == DECK_SIZE
        end
        # if filled up
        break if self.decks[player].size == DECK_SIZE
        new_card = Card.get_by_pack(self.packs)[:white].sample.text
        self.decks[player] << new_card if !self.decks[player].include?(new_card)
      end
    end

    self.save!
  end

  def waiting_for_players
    self.round.reject do |k, v|
      # where played cards = cards required to be picked by black card
      v.length == (Card.where("id=?", self.black_card).first.pick rescue 0)
    end.keys # only return names
  end

  def migrate_nick(from, to)
    update_players(to)

    self.decks[to] = self.decks[from]
    self.round[to] = self.round[from]
    self.scores[to] = self.scores[from]

    # migrate states from old player
    self.host = to if self.host == from
    self.card_czar = to if self.card_czar == from
    self.last_kicked = to if self.last_kicked == from

    self.last_nick_change = [from, to]

    self.save!

    delete_player(from)
  end

  # Authentication and setting password
  def password=(password)
    self.password_digest = Password.create(password)
  end

  def authenticate(password)
    return self if password_digest.present? && Password.new(password_digest) == password
  end
end
