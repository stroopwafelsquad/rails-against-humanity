class Card < ActiveRecord::Base
  validates :pack, :color, :text, presence: true

  # class methods, weird syntax.. I know..
  class << self
    # get list of available packs
    def available_packs
      uniq.pluck(:pack) # get unique packs from all entries
    end

    # return hash with black and white cards from packs given
    def get_by_pack(*packs)
      total = {black: [], white: []}

      packs.each do |pack|
        total[:black].concat where(pack: pack, color: 'black')
        total[:white].concat where(pack: pack, color: 'white')
      end

      # looks cleaner to write 'return' instead of just 'total'
      return total
    end
  end
end
