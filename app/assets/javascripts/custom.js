function hideFlashes (delay) {
  setTimeout(function () {
    $('div.alert').last().slideUp(function () {
      $(this).remove(); // since we don't have .flash or similar
    });
  }, delay);
}

function showFlash(type, msg) {
  $(".flashes").prepend("<div style='display:none;' class='alert flash-" +
                         type +
                         "' role='alert'><button type='button' class='close'>\
                         <span aria-hidden='true'>&times;</span>\
                         </button>" + msg + "</div>");
  $('.flash-' + type).slideDown();
  $('.alert,.alert button').off('click').on('click', function () {
    hideFlashes(0);
  });
  hideFlashes(5000);
}

updateComponents = ["players", "cards", "games"];

function submitOnClick(tags) {
  $(tags).off('click');
  $(tags).on('click', function () {
    $(this).parent('form').submit();
  });
}

function submitForms () {
  $(updateComponents).each(function (i,e) {
    if ($('form[action $= "' + e +'"]')) {
      $('form[action $= "' + e + '"]').trigger('submit.rails');
    }
  });
}

function ready () {
  submitOnClick('.game-card,.player-card-set');

  setInterval(submitForms, 3000);

  hideFlashes(5000);

  $('.alert,.alert button').off('click').on('click', function () {
    hideFlashes(0);
  });

  $('div[data-protected="true"] .game-preview-info').off('click')
    .on("click", function () {
      $(this).parent().children(".collapse").slideToggle();
    });

  $('div[data-protected="false"] .game-preview-info').off('click')
    .on('click', function () {
      $(this).closest('form').submit();
    });

  $(".toggle-protected").on('click', function () {
    $('div[data-protected="true"]').slideToggle();
  });
}

$(document).ready(ready);
$(document).on('page:load', ready);
