module ApplicationHelper
  def full_title(page='')
    page.empty? ? "Rails Against Humanity" : "#{page} | RAH"
  end

  def active_class(action_name, name)
    action_name == name ? "class=active" : ""
  end
end
