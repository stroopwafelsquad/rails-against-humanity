# Rails Against Humanity

## Disclaimer
**THIS PROGRAM HAS BEEN DISCONTINUED. IF YOU ARE GOING TO USE THIS PROGRAM, PLEASE BE AWARE THAT YOUR EXPERIENCE WILL BE SEVERELY IMPACTED BY NUMEROUS BUGS UNLIKE ANYTHING YOU HAVE EVER WITNESSED.**

## Description

This is a clone of the popular _Cards Against Humanity_ clone _Pretend You're Xyzzy_.

Authors are in the AUTHORS file, and the licence is in the LICENCE file.

## Requirements

We have developed the whole application using Ruby on Rails 4, and the specs are written using the default testing framework for rails.

That means that the only things you need are:

* ruby >= 2.1.0
* rails >= 4.2.0.0
* rspec >= 3.4.0

## Running _Rails Against Humanity_

`rails server` should do it. Naturally after you've ran `bundle install`. (I think)

## Contribution

As of right now you may not contribute to the code-base,
but when the program gets a bit more mature,
we will open it up the the general public for contribution.
